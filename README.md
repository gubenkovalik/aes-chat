1. Copy .env.example to .env
2. Change variables in .env if needed
3. Run `./build.sh`
4. Run `./start.sh`
5. Go to `http://localhost:4208`
6. Do some security using this nginx config:
    ```nginx
    server {
        server_name yourdomain.com;
    
        listen 80;
        
        location / {
                proxy_pass http://127.0.0.1:4208;
        }
    
        location /socket.io/ {
            proxy_pass http://127.0.0.1:4208/socket.io/;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header Host $http_host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_read_timeout 999999999;
        }
    }
    ```
7. Run `letsencrypt -d yourdomain.com`
8. ...
9. Profit!!1

_dont forget to change domain if needed; steps 6 and 7 are not necessary_
