#!/bin/sh
set -e
echo "{\"connectionString\": \"mongodb://${MONGO_INITDB_ROOT_USERNAME}:${MONGO_INITDB_ROOT_PASSWORD}@db:27017/cryptobak?authSource=admin\"}" > config.json
echo "Created config.json"


if [ "${1#-}" != "${1}" ] || [ -z "$(command -v "${1}")" ]; then
  set -- node "$@"
fi

exec "$@"