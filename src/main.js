const PORT = 4242;

let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);

const mongoose = require('mongoose');
const db = require('./db');

var msgSchema = new mongoose.Schema({
    chat_hash: { type: String, required: true },
    username: { type: String, required: true },
    message: { type: String, required: true },
    createdDate: { type: Date, default: Date.now }
});

var Message = mongoose.model('Message', msgSchema);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/static/index.html')
});


http.listen(PORT, () => {
    console.log('Listening on port *: '+PORT);
});

let chat_connections = {};

const updateConnections = function(chat_hash, modifier) {
    if (!chat_connections.hasOwnProperty(chat_hash)) {
        chat_connections[chat_hash] = 0;
    }

    chat_connections[chat_hash] += modifier;

    return chat_connections[chat_hash];
};

io.on('connection', (socket) => {

    socket.emit('connections', Object.keys(io.sockets.connected).length);

    socket.on('disconnect', () => {
        console.log("A user disconnected");
    });


    socket.on('chat-message', (data) => {
        console.log('Message: ');
        console.log(data);
        const msg = new Message();
        msg.chat_hash = data.chat_hash;
        msg.username = data.user;
        msg.message = data.message;
        msg.save();
        data.created = msg.createdDate;
        socket.broadcast.emit(data.chat_hash+'.chat-message', (data));
    });

    socket.on('typing', (data) => {
        socket.broadcast.emit(data.chat_hash+'.typing', (data));
    });

    socket.on('stopTyping', (data) => {
        socket.broadcast.emit(data.chat_hash+'.stopTyping');
    });

    socket.on('joined', (data) => {

        var connnow = updateConnections(data.chat_hash, 1);
        console.log('Connections now : '+connnow);
        console.log('Emit joined');
        socket.emit(data.chat_hash+'.joined', (data));


        console.log('Emit connection');
        socket.emit(data.chat_hash+'.connections', connnow);
        console.log('BCST connection');
        socket.broadcast.emit(data.chat_hash+'.connections', connnow);

        Message.find({ chat_hash: data.chat_hash }).exec(function(err, res){
            console.log('emiting history');
            socket.emit(data.chat_hash+".history", res);
        });



    });

    socket.on('leave', (data) => {
        var connnow = updateConnections(data.chat_hash, -1);
        socket.broadcast.emit(data.chat_hash+'.leave', (data));
        socket.emit(data.chat_hash+'.connections', connnow);
        socket.broadcast.emit(data.chat_hash+'.connections', connnow);
    });

});